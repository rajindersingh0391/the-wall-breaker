﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameSession : MonoBehaviour
{
    [Range(0.1f,10f)] [SerializeField] float gameSpeed = 1f;  // use Range so your values stays in a certain limit
    [SerializeField] int pointsPerBlockDestroyed = 83;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] bool isAutoPlayEnabled;

    int? currentScore = 0; //Explaination of ? -> https://stackify.com/nullreferenceexception-object-reference-not-set/

    private void Awake()
    {
        int gameStatusCount = FindObjectsOfType<GameSession>().Length;
        if(gameStatusCount > 1)
        {
            // Game object gets detroyed at end of lifecycle, so there could be a chance that there is more than one object for a split second.
            //That is the reason we make the game object inactive so its stops working and then gets destroyed afterwards. https://docs.unity3d.com/Manual/ExecutionOrder.html
            gameObject.SetActive(false); 
            Destroy(gameObject); // When game jumps to level 2, its destroys its Game Status object
        }
        else
        {
            DontDestroyOnLoad(gameObject); // When game jumpts to level 2, it sees it only has one object which is retained
        }
    }

    private void Start()
    {
        scoreText.text = currentScore.ToString();   
    }

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = gameSpeed;
        
    }

    public void AddToScore()
    {
        currentScore += pointsPerBlockDestroyed;
        scoreText.text = currentScore.ToString();
    }

    public void ResetGame()
    {
        Destroy(gameObject);
    }

    public bool IsAutoPlayEnabled()
    {
        return isAutoPlayEnabled;
    }
}
