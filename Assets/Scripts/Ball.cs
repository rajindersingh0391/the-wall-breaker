﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    // config params
    [SerializeField] private Paddle paddle1;
    [SerializeField] float xPush = 2f;
    [SerializeField] float yPush = 10f;
    [SerializeField] AudioClip[] ballSounds;
    [SerializeField] float randomFactor = 0.2f;
    [SerializeField] float speed = 1;

    Vector2 paddleToBallVector;
    bool ballReleased = false;

    // Cached references
    AudioSource audioSource;
    Rigidbody2D myRigidbody2D;

    // Start is called before the first frame update
    void Start()
    {
        // distance between paddel & ball
        paddleToBallVector = transform.position - paddle1.transform.position;
        //find the audio source component
        audioSource = GetComponent<AudioSource>();
        myRigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame (If ball passes through paddle or walls at high speed, try using FixedUpdate() method instead
    void Update()
    {
        if (!ballReleased)
        {
            LockBallToPaddle();
            LaunchBallOnMouseClick();
        }
    }

    private void LaunchBallOnMouseClick()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            myRigidbody2D.velocity = new Vector2(xPush, yPush);
            ballReleased = true;
        }
    }

    private void LockBallToPaddle()
    {
        Vector2 paddlePos = new Vector2(paddle1.transform.position.x, paddle1.transform.position.y);
        transform.position = paddlePos + paddleToBallVector;
    }

    // As soon as ball collides with something, below method is invoked
    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Vel: " + myRigidbody2D.velocity.magnitude);
        Vector2 velocityTweak = new Vector2(Random.Range(0f, randomFactor), Random.Range(0f, randomFactor));

        if (ballReleased)
        {
            AudioClip audioClip = ballSounds[UnityEngine.Random.Range(0, ballSounds.Length)]; // each time collision happens, we will get a random audio clip to be played
            audioSource.PlayOneShot(audioClip); //whatever sound started finish it, if other audio starts it will overlap
            Vector2 v = myRigidbody2D.velocity.normalized;
            v *= speed;
            v += velocityTweak;
            myRigidbody2D.velocity += v;
        }
    }
}