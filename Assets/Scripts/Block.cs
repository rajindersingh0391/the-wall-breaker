﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    // config params
    [SerializeField] AudioClip breakSound;
    [SerializeField] GameObject blockSparklesVFX;
    [SerializeField] Sprite[] hitSprites;

    // cached reference
    Level level;

    // state variables
    int timesHit;
    
    private void Start()
    {
        CountBreakableBlocks();
    }

    private void CountBreakableBlocks()
    {
        level = FindObjectOfType<Level>();
        if (tag == "Breakable")
        {
            level.CountBlocks();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (tag == "Breakable")
        {
            HandleBlockHit();
        }
    }

    private void HandleBlockHit()
    {
        timesHit++;
        int maxHits = hitSprites.Length + 1;
        if (timesHit >= maxHits)
        {
            DestroyBlock();
        }
        else
        {
            ShowNextHitSprite(timesHit);
        }
    }

    private void ShowNextHitSprite(int timesHit)
    {
        int spriteIndex = timesHit - 1;
        if (hitSprites[spriteIndex] != null)
        {
            GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
        else
        {
            Debug.LogError("Block Sprite is missing from array!" + gameObject.name); //Lists down the exact block from where sprite is missing
        }
    }

    private void DestroyBlock()
    {
        //Debug.Log(collision.gameObject.name); // gets the name of the object which collided with Block.
        AudioSource.PlayClipAtPoint(breakSound, Camera.main.transform.position, 1);
        Destroy(gameObject);

        level.BlockDistroyed(); // decrease the block count as one block got destroyed
        FindObjectOfType<GameSession>().AddToScore(); // adds points to CurrentScore as soon as the block is destroyed

        TriggerSparklesVFX();
    }

    private void TriggerSparklesVFX()
    {
        GameObject sparkles = Instantiate(blockSparklesVFX, transform.position, transform.rotation);
        Destroy(sparkles, 1f);
    }
}
